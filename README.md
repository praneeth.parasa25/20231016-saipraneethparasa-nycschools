**Assumptions:**

* The School SAT scores are found by "dbn" field from School's List API.
* Displaying the Required Details like School name, Email and Phone Number on home page.
* If the Internet goes down then the next screen would be recomposed and would be prompted to retry after active internet.
* Created Cards for both Portrait and Landscape views in mind.
* Schools SAT Card design accompanies Card content scrolling incase of Landscape view if data is overgrown.
* Both Screen support "No Internet Connection" with a retry button to get the data when online.
* Implemented MVVM.
* Implemented Hilt Dependency Injection.
* Implemented Unit test cases for View Model, repository's and Api Service calls.
* Implemented with Kotlin and used kotlin tools.
* Implemented Jetpack Compose Ui Toolkit.
* Error Handling for Repository's and View Model.


**Screen recording for the application**
https://gitlab.com/praneeth.parasa25/20231016-saipraneethparasa-nycschools/-/blob/main/Android_App_Recording.mp4?ref_type=heads
